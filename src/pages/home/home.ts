import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { NavController, Platform, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';
import { LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

import { Sim } from '@ionic-native/sim';
import { Device } from '@ionic-native/device';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  logoImage: string = "";
  escondePrincipal: boolean = false;
  escondeEntrar: boolean = true;
  escondeCriar: boolean = true;
  numeroSala: any;
  hourtime: any = '00';
  minutetime: any = '00';
  secondtime: any = '00';
  simpleColumns: any = [];
  tempo: any;
  loadingEvent: any;
  conectadoInternet: boolean = true;

  apiUrl = 'https://chirpservices-juliogrativol763657.codeanyapp.com';

  constructor(public navCtrl: NavController, platform: Platform, public navParams: NavParams,
    public http: HttpClient, private storage: Storage, private network: Network,
    public loadingCtrl: LoadingController, private alertCtrl: AlertController, private sim: Sim,
    private device: Device) {
    platform.ready().then(() => {

      this.storage.set('responsavelPelaSala', false);
      this.storage.set('numeroSala', undefined);
      this.storage.set('tokenResponsavel', undefined);

      if (platform.is('android')) {
        this.logoImage = '/android_asset/www/assets/imgs/logopassaro.svg';
      }
      if (platform.is('ios')) {
        this.logoImage = './assets/imgs/logopassaro.svg';
      }

      this.carregaValoresTempo();

      // watch network for a disconnect
      let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
        this.conectadoInternet = false;
      });

      // watch network for a connection
      let connectSubscription = this.network.onConnect().subscribe(() => {
        this.conectadoInternet = true;
        setTimeout(() => {
          this.conectadoInternet = true;
        }, 3000);
      });

      let loading = this.loadingCtrl.create({
        content: 'Aguarde...'
      });

      this.loadingEvent = loading;

      this.verificaSalasAbertas();

    });
  }

  verificaSalasAbertas() {

    this.loadingEvent.present();

    this.storage.get('idDispositivoFCM')
      .then((idDispositivoFCM) => {
        console.log('idDispositivoFCM', idDispositivoFCM);

        this.http.get(this.apiUrl + '/salaCriada/' + idDispositivoFCM)
          .toPromise()
          .then((response) => {

            console.log('chamou o serviço', response['id']);

            if (response['id'] != undefined) {
              let alert = this.alertCtrl.create({
                title: 'Alerta',
                message: 'A sala de reunião ' + response['id'] + ' ficou aberta. Você precisa encerrá-la para criar outra.',
                buttons: [
                  {
                    text: 'Encerrar',
                    role: 'encerrar',
                    handler: () => {
                      console.log('Encerrando sala aberta');
                      this.http.post(this.apiUrl + '/encerrar', { "id": response['id'] })
                        .toPromise()
                        .then((response) => {
                          console.log("Sala encerrada", response['id']);
                          this.alertCtrl.create({
                            title: 'Informação',
                            subTitle: 'Sala encerrada com sucesso!.',
                            buttons: ['OK']
                          }).present();
                          this.numeroSala = undefined;
                          this.storage.set('numeroSala', undefined);
                          this.storage.set('tokenResponsavel', undefined);
                          this.storage.set('responsavelPelaSala', false);
                          this.loadingEvent.dismiss();
                        })
                        .catch((error) => {
                          this.loadingEvent.dismiss();
                          this.alertCtrl.create({
                            title: 'Falha',
                            subTitle: 'Problemas ao encerrar sala reunião.',
                            buttons: ['OK']
                          }).present();
                        });
                    }
                  }
                ]
              });
              alert.present();
            } else {
              this.loadingEvent.dismiss();
            }
          })
          .catch((error) => {
            this.loadingEvent.dismiss();
            this.alertCtrl.create({
              title: 'Falha',
              subTitle: 'Falha ao buscar salas abertas para o dispositivo.',
              buttons: ['OK']
            }).present();
          });
      }).catch(function (error) {
        this.loadingEvent.dismiss();
        console.log("Houve uma falha ao buscar o id do dispositivo");
        this.alertCtrl.create({
          title: 'Falha',
          subTitle: 'Houve uma falha interna.',
          buttons: ['OK']
        }).present();
      });
  }

  openDetails(acao: String) {
    if (acao === 'entrar') {
      if (this.conectadoInternet) {
        if (this.numeroSala == undefined) {
          this.alertCtrl.create({
            title: 'Campo obrigatório',
            subTitle: 'Preencha o número da sala!',
            buttons: ['OK']
          }).present();
        } else {
          this.loadingEvent.present();
          this.http.get(this.apiUrl + '/sala/' + this.numeroSala)
            .toPromise()
            .then((response) => {
              if (response['id_reponsavel'] != undefined) {
                this.storage.set('tokenResponsavel', response['id_reponsavel']);
                this.storage.set('numeroSala', response['id']);
                this.escondePrincipal = true;
                this.escondeEntrar = false;
                this.escondeCriar = true;

                let tempoRestante = response['tempoRestante'];

                //definir o tempo
                let minutos = 0;
                let horas = 0;

                this.secondtime = this.duas_casas(0);
                this.minutetime = this.duas_casas(0);
                this.hourtime = this.duas_casas(0);

                if (tempoRestante > 60) {

                  this.secondtime = this.duas_casas((tempoRestante % 60));

                  minutos = Math.floor(tempoRestante / 60);

                  if (minutos > 59) {
                    this.minutetime = this.duas_casas(Math.floor(minutos % 60));
                    this.hourtime = this.duas_casas(Math.floor(minutos / 60));
                  } else {
                    this.minutetime = this.duas_casas(minutos);
                  }

                } else {
                  this.secondtime = this.duas_casas(tempoRestante);
                }

                this.loadingEvent.dismiss();
                this.startTimer();
              } else {
                this.loadingEvent.dismiss();
                this.alertCtrl.create({
                  title: 'Falha',
                  subTitle: 'Sala não encontrada!',
                  buttons: ['OK']
                }).present();
              }
            })
            .catch((error) => {
              this.loadingEvent.dismiss();
              this.alertCtrl.create({
                title: 'Falha',
                subTitle: 'Problemas na busca da sala!',
                buttons: ['OK']
              }).present();
            });
        }
      } else {
        this.alertCtrl.create({
          title: 'Falha',
          subTitle: 'Verifique sua conexão com a Internet!',
          buttons: ['OK']
        }).present();
      }
    }
    if (acao === 'criar') {
      this.escondePrincipal = true;
      this.escondeEntrar = true;
      this.escondeCriar = false;
    }
  }

  duas_casas(numero) {
    if (numero <= 9) {
      numero = "0" + numero;
    }
    return numero;
  }

  startTimer() {
    setTimeout(x => {

      if (parseInt(this.secondtime) == 0) {
        if (parseInt(this.minutetime) > 0) {
          if (this.minutetime <= 10) {
            this.minutetime = '0' + (this.minutetime - 1);
          } else {
            this.minutetime -= 1;
          }
          this.secondtime += 60;
        } else {
          if (parseInt(this.hourtime) > 0) {
            if (this.hourtime <= 10) {
              this.hourtime = '0' + (this.hourtime - 1);
            } else {
              this.hourtime -= 1;
            }
            this.minutetime = 59;
            this.secondtime = 60;
          }
        }
      }

      if (this.secondtime > 0) {
        if (this.secondtime <= 10) {
          this.secondtime = '0' + (this.secondtime - 1);
        } else {
          this.secondtime -= 1;
        }
        this.startTimer();
      } else {
        this.storage.get('responsavelPelaSala')
          .then((responsavelPelaSala) => {

            if (responsavelPelaSala) {
              //encerrar a sala
              this.storage.get('numeroSala')
                .then((numeroSala) => {
                  console.log('numeroSala', numeroSala);
                  this.loadingEvent.present();
                  console.log('Encerrando sala aberta');
                  this.http.post(this.apiUrl + '/encerrar', { "id": numeroSala })
                    .toPromise()
                    .then((response) => {
                      console.log("Sala encerrada", response['id']);
                      this.alertCtrl.create({
                        title: 'Informação',
                        subTitle: 'Reunião encerrada com sucesso!.',
                        buttons: ['OK']
                      }).present();
                      this.numeroSala = undefined;
                      this.storage.set('numeroSala', undefined);
                      this.storage.set('tokenResponsavel', undefined);
                      this.storage.set('responsavelPelaSala', false);

                      this.escondePrincipal = false;
                      this.escondeEntrar = true;
                      this.escondeCriar = true;
                      this.loadingEvent.dismiss();
                    })
                    .catch((error) => {
                      this.loadingEvent.dismiss();
                      this.alertCtrl.create({
                        title: 'Falha',
                        subTitle: 'Problemas ao encerrar sala reunião.',
                        buttons: ['OK']
                      }).present();
                    });
                });
            } else {

              this.alertCtrl.create({
                title: 'Alerta',
                subTitle: 'Reunião Encerrada.',
                buttons: ['OK']
              }).present();

              this.numeroSala = undefined;
              this.storage.set('numeroSala', undefined);
              this.storage.set('tokenResponsavel', undefined);
              this.storage.set('responsavelPelaSala', false);

              this.escondePrincipal = false;
              this.escondeEntrar = true;
              this.escondeCriar = true;
              this.loadingEvent.dismiss();
            }
          });
      }
    }, 1000);
  }

  iniciar() {
    if (this.conectadoInternet) {
      if (this.tempo != undefined) {

        let tempoTotal = this.tempo.split(" ");

        this.hourtime = tempoTotal[0];
        this.minutetime = tempoTotal[1];

        this.loadingEvent.present();

        this.storage.get('idDispositivoFCM')
          .then((idDispositivoFCM) => {
            console.log('idDispositivoFCM', idDispositivoFCM);

            let tempo = (parseInt(this.hourtime) * 60) + parseInt(this.minutetime);

            console.log("Tempo", tempo);

            this.http.post(this.apiUrl + '/sala', { "id_responsavel": idDispositivoFCM, "tempo": tempo })
              .toPromise()
              .then((response) => {
                console.log("sala Criada");
                this.numeroSala = response['id'];
                this.escondePrincipal = true;
                this.escondeEntrar = false;
                this.escondeCriar = true;

                this.storage.set('responsavelPelaSala', true);
                this.storage.set('numeroSala', response['id']);
                this.storage.set('tokenResponsavel', response['id_reponsavel']);

                this.http.get(this.apiUrl + '/sala/' + this.numeroSala)
                  .toPromise()
                  .then((response) => {
                    if (response['id_reponsavel'] != undefined) {
                      this.storage.set('tokenResponsavel', response['id_reponsavel']);
                      this.storage.set('numeroSala', response['id']);
                      this.escondePrincipal = true;
                      this.escondeEntrar = false;
                      this.escondeCriar = true;

                      let tempoRestante = response['tempoRestante'];

                      //definir o tempo
                      let minutos = 0;
                      let horas = 0;

                      this.secondtime = this.duas_casas(0);
                      this.minutetime = this.duas_casas(0);
                      this.hourtime = this.duas_casas(0);

                      if (tempoRestante > 60) {

                        this.secondtime = this.duas_casas((tempoRestante % 60));

                        minutos = Math.floor(tempoRestante / 60);

                        if (minutos > 59) {
                          this.minutetime = this.duas_casas(Math.floor(minutos % 60));
                          this.hourtime = this.duas_casas(Math.floor(minutos / 60));
                        } else {
                          this.minutetime = this.duas_casas(minutos);
                        }

                      } else {
                        this.secondtime = this.duas_casas(tempoRestante);
                      }

                      this.loadingEvent.dismiss();
                      this.startTimer();
                    } else {
                      this.loadingEvent.dismiss();
                      this.alertCtrl.create({
                        title: 'Falha',
                        subTitle: 'Sala não encontrada!',
                        buttons: ['OK']
                      }).present();
                    }
                  })
                  .catch((error) => {
                    this.loadingEvent.dismiss();
                    this.alertCtrl.create({
                      title: 'Falha',
                      subTitle: 'Problemas na busca da sala!',
                      buttons: ['OK']
                    }).present();
                  });
                this.loadingEvent.dismiss();
              })
              .catch((error) => {
                this.loadingEvent.dismiss();
                this.alertCtrl.create({
                  title: 'Falha',
                  subTitle: 'Houve uma falha iniciar a reunião.',
                  buttons: ['OK']
                }).present();
              });
          }).catch(function (error) {
            this.loadingEvent.dismiss();
            console.log("Houve uma falha ao buscar o id do dispositivo");
            this.alertCtrl.create({
              title: 'Falha',
              subTitle: 'Houve uma falha interna.',
              buttons: ['OK']
            }).present();
          });
      } else {
        this.loadingEvent.dismiss();
        this.alertCtrl.create({
          title: 'Campo Obrigatório',
          subTitle: 'Defina o tempo.',
          buttons: ['OK']
        }).present();
      }
    } else {
      this.alertCtrl.create({
        title: 'Falha',
        subTitle: 'Verifique sua conexão com a Internet!',
        buttons: ['OK']
      }).present();
    }
  }

  carregaValoresTempo() {
    let optHoras = [];
    let optMinutos = [];

    for (let i = 0; i <= 5; i++) {
      let obj = {
        text: '',
        value: ''
      };

      if (i < 10) {
        obj.text = String('0' + i + ' h');
        obj.value = String('0' + i);
      } else {
        obj.text = String(i + ' h');
        obj.value = String(i);
      }

      optHoras.push(obj);
    }

    for (let i = 1; i <= 60; i++) {
      let obj = {
        text: '',
        value: ''
      };

      if (i < 10) {
        obj.text = String('0' + i + ' min');
        obj.value = String('0' + i);
      } else {
        obj.text = String(i + ' min');
        obj.value = String(i);
      }

      optMinutos.push(obj);
    }

    this.simpleColumns = [
      {
        name: 'col1',
        options: optHoras
      }, {
        name: 'col2',
        options: optMinutos
      }
    ];
  }
}

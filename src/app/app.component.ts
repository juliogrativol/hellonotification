import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Observable } from "rxjs/Rx";

import { HomePage } from '../pages/home/home';

import { FCM } from '@ionic-native/fcm'
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = HomePage;
  apiUrl = 'https://chirpservices-juliogrativol763657.codeanyapp.com';

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private fcm: FCM,
    private storage: Storage, public http: HttpClient, private alertCtrl: AlertController) {

    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();

      this.fcm.getToken().then(token => {
        console.log('token do dispositivo : ' + token);
        this.storage.set('idDispositivoFCM', token);
      });

      this.fcm.onTokenRefresh().subscribe(token => {
        console.log('token do dispositivo : ' + token);
        this.storage.set('idDispositivoFCM', token);
      });

      this.fcm.onNotification().subscribe(data => {
        this.alertCtrl.create({
          title: 'Perda de Foco',
          subTitle: 'Alguém perdeu o foco.',
          buttons: ['OK']
        }).present();
        console.log('mensagem : ' + data);
        if (data.wasTapped) {

        } else {

        };
      });


      platform.resume.subscribe((e) => {

      });

      platform.pause.subscribe((e) => {

        this.storage.get('numeroSala')
          .then((numeroSala) => {
            console.log('Numero Sala no storage: ' + numeroSala);
            if (numeroSala != undefined) {

              this.storage.get('responsavelPelaSala')
                .then((isResponsavelPelaSala) => {

                  console.log('É responsavel pela sala: ' + isResponsavelPelaSala);

                  if (!isResponsavelPelaSala) {
                    this.storage.get('tokenResponsavel')
                      .then((tokenResponsavel) => {
                        console.log('tokenResponsavel no storage', tokenResponsavel);

                        //Buscar a sala para ver se existe...
                        this.http.get(this.apiUrl + '/sala/' + numeroSala)
                          .toPromise()
                          .then((response) => {

                            console.log('Busca da sala :' + response['id']);

                            if (response['id'] != undefined) {
                              this.http.post(this.apiUrl + '/notificar', { "id_responsavel": tokenResponsavel })
                                .toPromise()
                                .then((response) => {
                                  console.log("responsável Notificado!");
                                })
                                .catch((error) => {
                                  console.log("problemas ao Notificar!");
                                });
                            } else {
                              console.log('Sala não encontrada!');
                            }
                          });
                      }).catch(function (error) {
                        console.log("Houve uma falha ao buscar o id do dispositivo");
                      });
                  }
                });
            }
          });
      });

      platform.registerBackButtonAction(() => {

      }, 1);
    });
  }
}

